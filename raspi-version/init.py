#import organ_detection as organ
import plantnet_API as plantnet
import os
import cv2
import numpy as np
import time
#import picamera
import dialog # Pour les interfaces utilisateur avec tkinter
from move import *
from detect_organ import detect
import sys
sys.path.insert(0, os.getcwd())

#Cette fonction a pour but d'analyser grâce aux réseaux de neurones, ainsi que de donner et traiter les résultats

def organDetection(file, image_path, mode):
    start_time = time.time()
    net_weights_path = os.path.abspath("../weights") # Les chemins doivent être absolus
    net_config_path = os.path.abspath("../configurations")

    weights = net_weights_path + "/" + "best.pt"    # modèle pré-entraîné à utiliser


    # Chargement de l'image
    img=cv2.imread(image_path + file)
    height,width,channels=img.shape

    boxes_list = [] #les résultats : les coordonnées x y w h des organes de plantes détectés
    organ_list = [] #les résultats : le nom des organes détectés (feuille, fleur, etc.)

    label_list = [] #les organes détecté mais avec leur numéro et leur confiance
    if weights.endswith('.weights'):
        #charger le réseau de neurones créé avec YOLO
        print("Net scanning image...")



        net=cv2.dnn.readNet(weights, net_config_path + "/yolov4-plant.cfg")
        print("Images scanned")
        print("--- %s seconds ---" % (time.time() - start_time))


        #Pour charger tous les objets qui ont été détectés
        classes=["leaf", "flower"]
        #Si on a beaucoup de classes:
        # with open("robiome.names","r") as f:
        #     read=f.readlines()
        # for i in range(len(read)):
        #     classes.append(read[i].strip("\n"))

        # Definnision du nom des couches que YOLO a produit
        layer_names=net.getLayerNames()
        output_layers=[]
        for i in net.getUnconnectedOutLayers():
            output_layers.append(layer_names[i[0]-1])


        print("scoring...")

        # Extraits les éléments pour détecter les objets
        blob=cv2.dnn.blobFromImage(img,0.00392,(416,416),(0,0,0),True,crop=False)
                                               #Standard         #Inverting blue with red
                                               #ImageSize        #bgr->rgb


        # On a besoin de passer le img_blob dans notre algorithme
        net.setInput(blob)
        outs=net.forward(output_layers)
        print("scoring finished")
        print("--- %s seconds ---" % (time.time() - start_time))


        #Pour avoir les infos incrustées sur les images
        class_ids=[]
        confidences=[]
        boxes=[]

        for output in outs:
            for detection in output:
                # détecte la confiance en 3 étapes
                scores=detection[5:]                #1
                class_id=np.argmax(scores)          #2
                confidence =scores[class_id]        #3

                if confidence >0.5: # = si l'objet est détecté

                    center_x=int(detection[0]*width)
                    center_y=int(detection[1]*height)
                    w=int(detection[2]*width)
                    h=int(detection[3]*height)

                    # Dessine un rectangle
                    x=int(center_x-w/2) # top left value
                    y=int(center_y-h/2) # top left value

                    boxes.append([x,y,w,h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
                    # SI on veut déjà tout inscrire les résultats, mais il y aura peut-être des boxes en double
                    #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

        print("writing box info")
        # On supprime les doublons de box avec l'algo de Non Maximum Suppression de cv2.dnn
        indexes=cv2.dnn.NMSBoxes(boxes,confidences,0.3,0.4)
        #defining x, y, weight and height lists and label list to record multiple boxes and their label

        count = len(boxes)
        for i in range(len(boxes)):
            print(count)
            if i in indexes:
                x, y, w, h = boxes[i]
                organ_detected = classes[class_ids[i]]
                label = "#" + str(count+1) + " " + organ_detected + ": " + str(int(confidences[i]*100)) + "%"  # name of the objects ('#x organ: xxx%')
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 3)
                # put text with border (so x2)
                coordYTxt = y + 40 + h if y + h + 40 < height else y + h
                cv2.putText(img, label, (x, coordYTxt), cv2.FONT_HERSHEY_PLAIN, 4, (0, 0, 0), 7)
                cv2.putText(img, label, (x, coordYTxt), cv2.FONT_HERSHEY_PLAIN, 4, (255, 255, 0), 4)
                print("Writing box info on image")

                # cv2.imshow("Appuyer pour continuer",img)
                # print("Writing box info on image")
                # cv2.waitKey(0)
                #
                # cv2.setMouseCallback("Appuyer pour continuer", click)


                #cv2.imwrite(image_path + "result_" + file, img) #this line should be temporary

                boxes_list.append(boxes[i])
                organ_list.append(organ_detected)
                label_list.append(label)
                print("--- %s seconds ---" % (time.time() - start_time))

                print(file, ":", label + " detected")
                cv2.imwrite(image_path + "result_" + file, img)

                count += 1
            else:
                count -= 1 # pour voir si au final on a quelque chose de détecté au moins ou pas

    elif weights.endswith('.pt'):
        results = detect(weights, image_path + file, 640, 0.25)
        boxes = results[0]
        labels = results[1]
        confs = results[2]

        if boxes:
            for i in range(len(boxes)):
                x, y, w, h = boxes[i]
                organ_detected = labels[i]
                label = "#" + str(i+1) + " " + organ_detected + ": " + str(confs[i]) + "%"  # name of the objects ('#x organ: xxx%')
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 5)
                # put text with border (so x2)
                coordYTxt = y + 40 + h if y + h + 40 < height else y + h
                cv2.putText(img, label, (x, coordYTxt), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 7)
                cv2.putText(img, label, (x, coordYTxt), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 4)
                print("Writing box info on image")



                # cv2.imshow("Appuyer pour continuer",img)
                # print("Writing box info on image")
                # cv2.waitKey(0)
                #
                # cv2.setMouseCallback("Appuyer pour continuer", click)



                boxes_list.append(boxes[i])
                organ_list.append(organ_detected)
                label_list.append(label)

                print("--- %s seconds ---" % (time.time() - start_time))

                print(file, ":", label + " detected")
                cv2.imwrite(image_path + "result_" + file, img)








    # partie tranfert à plantnet / résultat
    results = []
    path = image_path + "result_" + file
    if boxes_list:
        if mode == "user":
            if os.path.isfile(path):
                answer = dialog.approach_or_not(file, image_path, len(boxes_list))
                i = int(answer["nb"])
                if answer["found"] == "oui":
                    x, y, w, h = boxes_list[i-1]
                    test_where_to_go(x, y, w, h, height, width)
                    results.append({"state": "approaching", "coord": None, "species": None})
                    return results
                elif answer["found"] == "non":
                    print(file + " : detection finished")
                    result_names = plantnet.recognition(file, image_path, organ_list[i-1])

                    for i, line in enumerate(result_names.split('\n')): # boucle pour chaque espèce reconnue, séparé dans le fonction plantnet par un retour à la ligne '\n'
                        cv2.putText(img, line, (10, 10 + 40*(i+2)), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 7)
                        cv2.putText(img, line, (10, 10 + 40*(i+2)), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 4)
                    cv2.imwrite(path, img)
                    #write results to file
                    with open(image_path + "result_" + file[:-3] + "txt", 'a') as f:
                        f.write(label_list[i-1])
                        f.write('\n')
                        f.write(result_names)
                        f.write('\n')

                    results.append({"state": "found", "coord": boxes_list[i-1], "species": result_names})
                    return results
            elif count == 0:  # pour voir si au final on a quelque chose de détecté au moins ou pas
                print(file + " : nothing detected")
                results.append({"state": "not found", "coord": None, "species": None})
                return results
        elif mode == "auto":
            print("test")
            k = 0
            for box in boxes_list:
                x,y,w,h = box
                if w<200:
                    w=250
                    x-=50
                if h<200:
                    h=250
                    y-=50
                if y<0:
                    y=0
                if y+h>height:
                    h= height-y
                if x<0:
                    x=0
                if w+x>width:
                    w=width-x

                crop_img = cv2.imread(image_path + file)
                crop_img = crop_img[y:y+h, x:x+w]

                cv2.imwrite(image_path + "tmp.jpg", crop_img)

                result_names = plantnet.recognition("tmp.jpg", image_path, organ_list[k])
                # for i, line in enumerate(result_names.split('\n')):
                #     cv2.putText(img, line, (x +10, y + 20 + 40*(i+2)), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 7)
                #     cv2.putText(img, line, (x + 10, y + 20 + 40*(i+2)), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 4)
                cv2.putText(img, f"See result_{file[:-3]}txt for results", (10, 10), cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 0), 7)
                cv2.putText(img, f"See result_{file[:-3]}txt for results", (10, 10), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 4)
                cv2.imwrite(path[:-4] + "_" + str(k) + ".jpg", crop_img)
                #write results to file
                with open(image_path + "result_" + file[:-3] + "txt", 'a') as f:
                    f.write(label_list[k])
                    f.write('\n')
                    f.write(result_names)
                    f.write('\n\n')

                results.append({"state": "found", "coord": box, "species": result_names})


                k += 1

    else:#if count == 0:  # pour voir si au final on a quelque chose de détecté au moins ou pas
        print(file + " : nothing detected")
        #write results to file
        results.append({"state": "not found", "coord": None, "species": None})

    return results



def init():

    global found
    found=0

    mode = "user"
    ######TEST avec photos telechargees
    # for i in range(24, 25):
    #
    #     organ_detected = organ.detection(str(i) + ".jpg", mode)
    #     if organ_detected != "false":
    #         print("Pl@ntNet.API loading...")
    #         plantnet.recognition(str(i) + ".jpg", image_path, organ_detected)

    while (found==0):
        if mode == "user":
            mode = dialog.which_mode()
        i=189 	# commencer normalement à 0

        ######## analyse d'un dossier local
        folder = "../../../acquisition_campaign"
        image_path = os.path.abspath(folder) + "/"
        print(image_path + str(i) + ".jpg")

        while(os.path.isfile(image_path + str(i) + ".jpg")):
            organ_detected = organDetection(str(i) + ".jpg", image_path, mode)
            i += 1




        ######### webcam usb
        # capture_command = "fswebcam " + image_path + str(i) + ".jpg"
        # os.system(capture_command)

        ######### picamera
        # camera = picamera.PiCamera()
        #
        # image_path = "../images/"
        # while(os.path.isfile(image_path + str(i) + ".jpg")): # on cherche à faire une base de données de fichiers-photos
        #     i += 1
        # camera = picamera.PiCamera()
        #
        # # on règle les picamera settings
        # camera.resolution = (2000, 1500)
        # camera.contrast = 10
        # camera.saturation = 35
        # camera.capture(image_path + str(i) + ".jpg")
        # camera.close()
        #
        # results = organDetection(str(i) + ".jpg", image_path, mode)

# pour lancer la fonction init
if __name__ == "__main__":
    init()
