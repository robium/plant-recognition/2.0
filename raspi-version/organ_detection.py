import cv2
import numpy as np
import time

def detection(file):
    start_time = time.time()
    net_weight_path = "/home/pi/Documents/robium/weights"
    net_config_path = "/home/pi/Documents/robium/configurations"

    #load YOLO created nets
    print("Net scanning image...")
    net=cv2.dnn.readNet(net_weight_path + "/yolov4-plant_best.weights", net_config_path + "/yolov4-plant.cfg")
    print("Images scanned")
    print("--- %s seconds ---" % (time.time() - start_time))


    #To load all objects that have to be detected
    classes=["leaf", "flower"]
    # with open("coco.names","r") as f:
    #     read=f.readlines()
    # for i in range(len(read)):
    #     classes.append(read[i].strip("\n"))

    #Defining layer names
    layer_names=net.getLayerNames()
    output_layers=[]
    for i in net.getUnconnectedOutLayers():
        output_layers.append(layer_names[i[0]-1])

    #Loading images

    img=cv2.imread("../images/" + file)
    height,width,channels=img.shape

    print("scoring...")

    #Extracting features to detect objects
    blob=cv2.dnn.blobFromImage(img,0.00392,(416,416),(0,0,0),True,crop=False)
                                           #Standard         #Inverting blue with red
                                           #ImageSize        #bgr->rgb


    #We need to pass the img_blob to the algorithm
    net.setInput(blob)
    outs=net.forward(output_layers)
    print("scoring finished")
    print("--- %s seconds ---" % (time.time() - start_time))


    #Displaying information on the screen
    class_ids=[]
    confidences=[]
    boxes=[]
    for output in outs:
        for detection in output:
            #Detecting confidence in 3 steps
            scores=detection[5:]                #1
            class_id=np.argmax(scores)          #2
            confidence =scores[class_id]        #3

            if confidence >0.5: #Means if the object is detected

                center_x=int(detection[0]*width)
                center_y=int(detection[1]*height)
                w=int(detection[2]*width)
                h=int(detection[3]*height)

                #Drawing a rectangle
                x=int(center_x-w/2) # top left value
                y=int(center_y-h/2) # top left value

                boxes.append([x,y,w,h])
                confidences.append(float(confidence))
                class_ids.append(class_id)
                #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

    ahead_right_small()
    print("writing box info")
    #Removing Double Boxes
    indexes=cv2.dnn.NMSBoxes(boxes,confidences,0.3,0.4)
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = classes[class_ids[i]]  # name of the objects
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # put text with border (so x2)
            cv2.putText(img, label, (x +10, y + 60), cv2.FONT_HERSHEY_PLAIN, 4, (0, 0, 0), 7)
            cv2.putText(img, label, (x + 10, y + 60), cv2.FONT_HERSHEY_PLAIN, 4, (255, 255, 0), 4)

            cv2.imshow("Output",img)
            print("Writing box info on image")

            ans = input("La position de la plante est-elle satisfaisante ? o/n")
            if (ans == "o" or ans == "O" or ans == "oui" or ans == "Oui"):
                found = 1

            print(file, ":", label + " detected")

            #then wirte image instead of showing for testing
            cv2.imwrite("../images/result_" + file, img)

            return label

    print(file + " : nothing detected")
    return "false"

    cv2.imwrite("../images/result_" + file, img)
    print("--- %s seconds ---" % (time.time() - start_time))

    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
