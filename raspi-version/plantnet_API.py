import requests
import json
from pprint import pprint
import time
import os

def recognition(file, image_path, organ):
    start_time = time.time()
    print("Pl@ntNet.API loading...")


    API_KEY = "2b10Fuz5zqdXWCxbotD61fCmwe"  # Set you API_KEY here
    api_endpoint = f"https://my-api.plantnet.org/v2/identify/all?api-key={API_KEY}"


    image_path_1 = os.path.abspath(image_path) + "/" + file
    image_data_1 = open(image_path_1, 'rb')

    #image_path_2 = "../data/image_2.jpeg"
    #image_data_2 = open(image_path_2, 'rb')


    data = {
        'organs': [organ]#, 'leaf']
    }

    files = [
        ('images', (image_path_1, image_data_1))#,
        #('images', (image_path_2, image_data_2))
    ]

    req = requests.Request('POST', url=api_endpoint, files=files, data=data)
    prepared = req.prepare()

    s = requests.Session()
    response = s.send(prepared)

    json_result = json.loads(response.text)

    pprint(response.status_code)
    pprint(json_result)
    print("--- %s seconds ---" % (time.time() - start_time))
    text_result = ""
    if len(json_result["results"]) >= 3:
    	Range = 3
    else:
    	Range = json_result["results"]
    for i in range(Range):
        nothing = True

        if (json_result["results"][i]["score"] > 0.05):
            nothing = False
            if json_result["results"][i]["species"]["commonNames"]:
                text_result += "Specie #" + str(i) + ": " + ", ".join(json_result["results"][i]["species"]["commonNames"]) + " (" + str(int(json_result["results"][i]["score"]*100)) + "%" + ")" + "\n"
                print(text_result)

            else:
                text_result += "Specie #" + str(i) + ": " + json_result["results"][i]["species"]["scientificName"] + " (" + str(int(json_result["results"][i]["score"]*100)) + "%" + ")" + "\n"
                print(text_result)

        if nothing and i == 0:
            print("The plant was recognized with too low probabilities.")
            text_result += "No results"

    if response.status_code != 200:

        raise Exception(str(response.json()))

    return text_result

if __name__ == '__main__':
    recognition("25.jpg", "leaf")
