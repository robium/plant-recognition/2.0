
def turn_a_little_right():
    print("Turning 30 degrees right")
def turn_right():
    print("Turning 60 degrees right")

def turn_a_little_left():
    print("Turning 30 degrees left")
def turn_left():
    print("Turning 60 degrees left")


def go_a_little_ahead():
    print("Going ahead of 0.5 meter")
def go_ahead():
    print("Going ahead of 1 meter")

def step_back():
    print("Going behind of 0.5 meter")

def test_where_to_go(x, y, w, h, height, width):
    if (y+h < height/6 or y > height/1.2 or h > height/1.3 or w > width/1.3):
        step_back()
    elif (x > width/1.2):
        turn_right()
    elif (x > width/1.5):
        turn_a_little_right()
    elif (x < width/6):
        turn_left()
    elif (x < width/3):
        turn_a_little_left()

    if (w < width/20 or h < height/14):
        go_ahead()
    elif (w < width/10 or h < height/7):
        go_a_little_ahead()
