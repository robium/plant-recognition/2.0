from tkinter import *
from PIL import ImageTk, Image

def oui():
    answer["found"] = "oui"
    answer["nb"] = int(variable.get())
    root.destroy()

def non():
    answer["found"] = "non"
    answer["nb"] = int(variable.get())
    root.destroy()

def approach_or_not(file, image_path, boxes_nb):
    global answer
    answer = {"found": "oui", "nb": 0}
    global root
    root = Tk()
    root.title("Approche de Robiome")
    root.after(60000, non)
    canvas = Canvas(root, width = 900, height = 600)
    canvas.pack()


    OPTIONS = []
    for i in range(boxes_nb):
        OPTIONS.append(i+1)

    global variable
    variable = StringVar(root)
    variable.set(OPTIONS[0]) # default value
    question1 = StringVar()
    question1.set("Voici les résultats d'analyse.\n\nSouhaitez-vous continuer à vous rapprocher\n d'une plante ?\n")
    question2 = StringVar()
    question2.set("Plante numéro :")

    l1 = Label(root,textvariable=question1)
    l1.pack()

    Button(text='oui, se rapprocher\nde la plante choisie', command=oui).pack(side=LEFT, padx=(30,5), pady=20)

    f = Frame(root, bd=0)
    f.pack(side=LEFT)
    l2 = Label(f, textvariable=question2)
    l2.pack(side=LEFT)
    w = OptionMenu(f, variable, *OPTIONS)
    w.pack(side=LEFT)

    #https://stackoverflow.com/questions/45441885/how-can-i-create-a-dropdown-menu-from-a-list-in-tkinter

    Button(text="non, soumettre l'image\nà reconnaissance", command=non).pack(side=RIGHT, padx=40, pady=20)

    path = image_path + "result_" + file
    img = Image.open(path)
    img = img.resize((800, 500), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    canvas.create_image(20,20, anchor=NW, image=img)
    mainloop()



    print("answer : %s %d" % (answer["found"], int(answer["nb"])))

    return answer

def user_mode():
    global mode
    mode = "user"
    root.destroy()

def auto_mode():
    global mode
    mode = "auto"
    root.destroy()


def which_mode():
    global root
    root = Tk()
    root.title("Choisissez un mode")
    global mode
    #mode = "auto"
    canvas = Canvas(root, width = 250, height = 50)
    canvas.pack()
    root.after(10000, auto_mode)

    question1 = StringVar()
    question1.set("Quel mode souhaitez-vous que Robiome utilise ?\n")

    l = Label(root, textvariable=question1)
    l.pack()

    #https://stackoverflow.com/questions/45441885/how-can-i-create-a-dropdown-menu-from-a-list-in-tkinter

    Button(text="Automatique", command=auto_mode).pack(side=RIGHT, padx=40, pady=20)
    Button(text="Utilisateur", command=user_mode).pack(side=LEFT, padx=40, pady=20)

    mainloop()

    print("--%s mode--\n" % mode)


    return mode
    
if __name__ == "__main__":
	approach_or_not("187.jpg", "../../../acquisition_campaign/", 21)	# tester une image
