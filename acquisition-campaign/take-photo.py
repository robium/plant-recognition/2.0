import os
import picamera

i=0


######### picamera

image_path = str(i) + ".jpg"

while(os.path.isfile(image_path)):
	i += 1
	image_path = str(i) + ".jpg"

camera = picamera.PiCamera()

camera.resolution = (2000, 1500)
camera.contrast = 10
camera.saturation = 35
camera.brightness = 60
camera.ISO = 600

camera.capture(image_path)

camera.close()
